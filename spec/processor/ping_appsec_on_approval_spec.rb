# frozen_string_literal: true

require 'spec_helper'

require_relative '../../triage/processor/ping_appsec_on_approval'
require_relative '../../triage/triage/event'

RSpec.describe Triage::PingAppSecOnApproval do
  include_context 'with event', 'Triage::MergeRequestEvent' do
    let(:approver_username) { 'approver' }
    let(:project_id) { Triage::Event::GITLAB_PROJECT_ID }
    let(:merge_request_iid) { 300 }
    let(:event_attrs) do
      {
        object_kind: 'merge_request',
        action: 'approval',
        from_gitlab_org?: true,
        jihu_contributor?: true,
        event_user_username: approver_username,
        iid: merge_request_iid,
        project_id: project_id
      }
    end
  end

  subject { described_class.new(event) }

  include_examples 'registers listeners', ['merge_request.approval', 'merge_request.approved']

  let(:merge_request_notes) do
    [
      { "body": "review comment 1" },
      { "body": "review comment 2" }
    ]
  end

  let(:comment_mark) do
    subject.__send__(:unique_comment).__send__(:hidden_comment)
  end

  before do
    stub_api_request(
      path: "/projects/#{project_id}/merge_requests/#{merge_request_iid}/notes?per_page=100",
      response_body: merge_request_notes)
  end

  describe '#applicable?' do
    context 'when no new pipeline approval comment will be made' do
      include_examples 'event is applicable'
    end

    context 'when event is not from gitlab-org' do
      before do
        allow(event).to receive(:from_gitlab_org?).and_return(false)
      end

      include_examples 'event is not applicable'
    end

    context 'when merge request is not a JiHu contribution' do
      before do
        allow(event).to receive(:jihu_contributor?).and_return(false)
      end

      include_examples 'event is not applicable'
    end

    context 'when there is already a comment for the same purpose' do
      let(:merge_request_notes) do
        [
          { "body": "review comment 1" },
          { "body": comment_mark }
        ]
      end

      include_examples 'event is not applicable'
    end
  end

  describe '#process' do
    context 'when merge request author is a JiHu contributor' do
      before do
        allow(event).to receive(:jihu_contributor?).and_return(true)
      end

      it 'posts a discussion to request AppSec review' do
        body = <<~MARKDOWN.chomp
          #{comment_mark}
          :wave: `@#{approver_username}`, thanks for approving this merge request.

          This is the first time the merge request is approved. Please wait for AppSec approval.

          cc @gitlab-com/gl-security/appsec this is a ~"JiHu contribution", please follow the [JiHu contribution review process](https://about.gitlab.com/handbook/ceo/chief-of-staff-team/jihu-support/jihu-security-review-process.html#security-review-workflow-for-jihu-contributions).
        MARKDOWN

        expect_discussion_request(event: event, body: body) do
          subject.process
        end
      end
    end
  end
end
