# frozen_string_literal: true

module Triage
  ClientError = Class.new(StandardError)
end
