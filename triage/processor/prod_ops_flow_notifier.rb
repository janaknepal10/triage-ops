# frozen_string_literal: true

require_relative '../triage/processor'
require_relative '../triage/unique_comment'

module Triage
  # When changes are done to Product Operations related pages, DRIs are asked for review.
  class ProdOpsFlowNotifier < Processor
    PROD_OPS_PATHS = [
      'sites/handbook/source/handbook/product-development-flow/',
      'sites/handbook/source/handbook/marketing/blog/release-posts/',
      '.gitlab/issue_templates/release-post-mvp-nominations.md',
      '.gitlab/issue_templates/Release-Post-Retrospective.md',
      '.gitlab/issue_templates/product-development-retro.md',
      '.gitlab/merge_request_templates/Change-Product-Handbook.md',
      '.gitlab/merge_request_templates/Release-Post-Bug-Performance-Usability-Improvement-Block.md',
      '.gitlab/merge_request_templates/Release-Post-Item.md',
      '.gitlab/merge_request_templates/Release-Post.md',
      'source/includes/_performance_'
    ].freeze

    react_to 'merge_request.update', 'merge_request.open'

    def applicable?
      event.from_www_gitlab_com? && prod_ops_related_change? &&
        unique_comment.no_previous_comment?
    end

    def process
      add_comment(review_request_comment)
    end

    private

    def review_request_comment
      comment = <<~MARKDOWN.chomp
        @brhea @fseifoddini please review this Product Operations related Merge Request.
      MARKDOWN

      unique_comment.wrap(comment).strip
    end

    def unique_comment
      @unique_comment ||= UniqueComment.new(self.class.name, event)
    end

    def prod_ops_related_change?
      merge_request_changes.any? do |change|
        prod_ops_related?(change)
      end
    end

    def merge_request_changes
      Triage.api_client.merge_request_changes(project_id, merge_request_iid).changes
    end

    def project_id
      event.project_id
    end

    def merge_request_iid
      event.iid
    end

    def prod_ops_related?(change)
      %w[old_path new_path].any? do |old_or_new|
        path = change[old_or_new]

        PROD_OPS_PATHS.any? do |page|
          path.start_with?(page)
        end
      end
    end
  end
end
